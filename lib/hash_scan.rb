class HashScan
  def scan(hash, key)
    raise ArgumentError unless hash.is_a?(Hash)

    hash[key]
  end
end
