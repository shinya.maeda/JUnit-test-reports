class Test
  def sum(a, b)
    a + b
  end

  def subtract(a, b)
    raise ArgumentError if b > a

    a + b
  end
end
